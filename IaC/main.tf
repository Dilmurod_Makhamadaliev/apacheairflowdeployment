terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configure the Microsoft Azure Provider
provider "azurerm" {
  features {}
}


resource "azurerm_resource_group" "AirflowResourceGroup" {
  name     = var.airflow_rg_name
  location = "East US"
}

resource "azurerm_service_plan" "AirflowAppServicePlan" {
  name = var.airflow_asp_name
  location = azurerm_resource_group.AirflowResourceGroup.location
  resource_group_name = azurerm_resource_group.AirflowResourceGroup.name
  os_type             = "Linux"
  sku_name            = "P1v2"
}

resource "azurerm_linux_web_app" "AirflowAppService" {
  name  = var.airflow_as_name
  location = azurerm_resource_group.AirflowResourceGroup.location
  resource_group_name = azurerm_resource_group.AirflowResourceGroup.name
  service_plan_id = azurerm_service_plan.AirflowAppServicePlan.id
  
  app_settings = {
    AIRFLOW__CORE__SQL_ALCHEMY_CONN = "postgresql://${azurerm_postgresql_server.PostgreSQLServer.administrator_login}@${azurerm_postgresql_server.PostgreSQLServer.name}:${azurerm_postgresql_server.PostgreSQLServer.administrator_login_password}@${azurerm_postgresql_server.PostgreSQLServer.name}.postgres.database.azure.com:5432/${azurerm_postgresql_database.PostgreSQLDatabase.name}"
    AIRFLOW__CORE__LOAD_EXAMPLES = "true"
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = true
  }
  
  site_config {
    always_on = "true"

    application_stack {
      docker_image = "DOCKER|puckel/docker-airflow:latest"
      docker_image_tag = "latest"
    }
  }
  
  
  
  identity {
    type = "SystemAssigned"
  }

}

resource "azurerm_postgresql_server" "PostgreSQLServer" {
  name                = var.airflow_PSQLserver_name
  location            = azurerm_resource_group.AirflowResourceGroup.location
  resource_group_name = azurerm_resource_group.AirflowResourceGroup.name

  sku_name   = "GP_Gen5_4"
  version    = "11"
  storage_mb = 640000

  administrator_login          = var.airflow_PSQLserver_admin
  administrator_login_password = var.airflow_PSQLserver_password

  auto_grow_enabled                 = true
  backup_retention_days             = 7
  geo_redundant_backup_enabled      = false
  infrastructure_encryption_enabled = false
  public_network_access_enabled     = true
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_postgresql_firewall_rule" "PostgreSQLServerFireWallRules" {
  name                = "office"
  resource_group_name = azurerm_resource_group.AirflowResourceGroup.name
  server_name         = azurerm_postgresql_server.PostgreSQLServer.name
  start_ip_address    = "40.112.8.12"
  end_ip_address      = "40.112.8.12"
}

resource "azurerm_postgresql_database" "PostgreSQLDatabase" {
  name                = var.airflow_PSQLdb_name
  resource_group_name = azurerm_resource_group.AirflowResourceGroup.name
  server_name         = azurerm_postgresql_server.PostgreSQLServer.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}