# CICD Required Codes (Draft)

Example of Build docker image and push it into Azure Container Registry

```yml
stages:
  - build
  - push

build:
  stage: build
  image: docker:19.03
  script:
    # Build Docker image from repository
    - docker build -t my-image .

push:
  stage: push
  image: docker:19.03
  script:
    # Login to Azure
    - az login -u $AZURE_USERNAME -p $AZURE_PASSWORD

    # Retrieve Azure Container Registry credentials
    - az acr credential show --name $AZURE_REGISTRY_NAME > registry-creds.json

    # Login to Azure Container Registry
    - docker login $AZURE_REGISTRY_NAME.azurecr.io --username $AZURE_USERNAME --password $(cat registry-creds.json | jq -r .passwords[0].value)

    # Push Docker image to Azure Container Registry
    - docker tag my-image $AZURE_REGISTRY_NAME.azurecr.io/my-image:$CI_COMMIT_REF_NAME
    - docker push $AZURE_REGISTRY_NAME.azurecr.io/my-image:$CI_COMMIT_REF_NAME

  only:
    - master
```

Example of building ACR Docker image in minikube. We need to make sure our image is successfully created and pushed to the ACR first

```yml
stages:
  - deploy

deploy:
  stage: deploy
  image: docker:19.03
  script:
    # Start Minikube cluster
    - minikube start --driver=none

   # Set Docker environment variables
    - eval $(minikube docker-env)

    # Login to Azure Container Registry
    - docker login $AZURE_REGISTRY_NAME.azurecr.io --username $AZURE_USERNAME --password $(cat registry-creds.json | jq -r .passwords[0].value)

    # Run Docker image from Azure Container Registry in Minikube cluster
    - docker run -p 8080:80 $AZURE_REGISTRY_NAME.azurecr.io/my-image:$CI_COMMIT_REF_NAME

  only:
    - master
```

Example of Helm Chart Creation form Azure Container Registery and push it to Kubernetes Cluster

```yml

stages:
  - build
  - deploy

build:
  stage: build
  image: alpine/helm:3
  script:
    # Login to Azure
    - az login -u $AZURE_USERNAME -p $AZURE_PASSWORD

    # Retrieve Azure Container Registry credentials
    - az acr credential show --name $AZURE_REGISTRY_NAME > registry-creds.json

    # Login to Azure Container Registry
    - helm registry login $AZURE_REGISTRY_NAME.azurecr.io --username $AZURE_USERNAME --password $(cat registry-creds.json | jq -r .passwords[0].value)

    # Create Helm chart from Azure Container Image
    - helm create my-chart
    - cd my-chart
    - sed -i "s/^name:.*$/name: my-chart/" Chart.yaml
    - sed -i "s/^version:.*$/version: 1.0.0/" Chart.yaml
    - sed -i "s/^description:.*$/description: A Helm chart for deploying an Azure Container Image/" Chart.yaml
    - echo "image:" >> values.yaml
    - echo "  repository: $AZURE_REGISTRY_NAME.azurecr.io/$AZURE_IMAGE_NAME" >> values.yaml
    - echo "  tag: $AZURE_IMAGE_TAG" >> values.yaml
    - helm package my-chart

  artifacts:
    paths:
      - my-chart-1.0.0.tgz

deploy:
  stage: deploy
  image: alpine/helm:3
  script:
    # Login to Azure
    - az login -u $AZURE_USERNAME -p $AZURE_PASSWORD

    # Install Helm chart into Kubernetes cluster
    - helm install my-chart-1.0.0.tgz --name my-release --cluster $KUBERNETES_CLUSTER_NAME

  only:
    - master
```